import { defineStore } from "pinia";

export interface ITask {
  done: boolean;
  fav: boolean;
  id: number;
  title: string;
}

export interface TaskState {
  List: ITask[];
}

export const useTaskStore = defineStore("tasks", {
  actions: {
    addTask(task: ITask) {
      this.List.push(task);
    },
    deleteTask(id: number) {
      const i = this.List.findIndex((t) => t.id === id);
      this.List.splice(i, 1);
    },
    toggleDone(id: number) {
      const i = this.List.findIndex((t) => t.id === id);
      this.List[i].done = !this.List[i].done;
    },
    toggleFav(id: number) {
      const i = this.List.findIndex((t) => t.id === id);
      this.List[i].fav = !this.List[i].fav;
    },
  },
  getters: {
    all: (state): ITask[] => state.List,
    allCount: (state): number => state.List.length,
    todo: (state): ITask[] => state.List.filter((t) => !t.done),
    todoCount: (state): number => state.List.filter((t) => !t.done).length,
  },
  state: (): TaskState => ({
    List: [
      { done: false, fav: false, id: 0, title: "Buy Milk" },
      { done: false, fav: true, id: 1, title: "Buy Cereal" },
      { done: false, fav: true, id: 2, title: "Eat Cereal" },
    ],
  }),
});

export class Task implements ITask {
  done: boolean = false;
  fav: boolean = false;
  id: number = 0;
  title: string = "";

  constructor(task?: ITask) {
    if (task) {
      Object.assign(this, task);
    }
  }

  static Add(t: ITask) {
    const store = useTaskStore();
    t.id = Math.max(...store.all.map((i) => i.id)) + 1;
    store.addTask({ ...t });
  }

  static Delete(id: number) {
    const store = useTaskStore();
    store.deleteTask(id);
  }

  static Count = () => {
    const store = useTaskStore();
    return {
      All: store.allCount,
      Todo: store.todoCount,
    };
  };

  static List = (): ITask[] => {
    const store = useTaskStore();
    return store.all;
  };

  static toggleDone = (id: number) => {
    const store = useTaskStore();
    store.toggleDone(id);
  };

  static toggleFav = (id: number) => {
    const store = useTaskStore();
    store.toggleFav(id);
  };
}
